﻿#include <iostream>

class Animal {
	public:
	virtual void voice() {
		std::cout << "Grr..";
	}
};

class Dog : public Animal {
	public:
	void voice()  {
		std::cout << "Woof\n";
	}
};

class Cat : public Animal {

public:
	void voice()  {
		std::cout << "Meow\n";
	}
};

class Bird : public Animal {
public:
	void voice() {
		std::cout << "Chik, Chik\n";
	}
};

int main() {
	Animal* zoo[3];
	zoo[0] = new Dog();
	zoo[1] = new Cat();
	zoo[2] = new Bird();
	for (int i = 0; i < 3; i++) {
		zoo[i]->voice();
	}
}
